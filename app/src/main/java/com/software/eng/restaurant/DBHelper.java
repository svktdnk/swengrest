package com.software.eng.restaurant;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sevket&mcanv on 19.04.2016.
 */

/**
 *
 */
public class DBHelper extends SQLiteOpenHelper  {

    private static final String DATABASE_NAME   = "RestaurantDB";
    private static final int SURUM =1;
    private static final String TABLE_YEMEKLER = "Yemekler";
    private static final String TABLE_PERSON = "Person";
    String password=" ";
    public DBHelper(Context context) {
        super(context, DATABASE_NAME ,null,SURUM);

    }

    /**
     *
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {


        String sql = "CREATE TABLE  " + TABLE_PERSON +  "(id INTEGER PRIMARY KEY AUTOINCREMENT ," +
                "personName TEXT , " +
                "personType TEXT , " +
                "personPassword TEXT ," +
                "personEmail TEXT " + ")";
        Log.d("DBHelper", "SQL : " + sql);


        try {

            db.execSQL(sql);
        }catch (SQLException e)
        {
            e.getCause();
        }
    }

    /**
     *
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PERSON);


        onCreate(db);
    }

    /**
     *
     * @param person
     */
    public void insertPerson(Person person) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("personName", person.getPersonName());
        values.put("personPassword", person.getPersonPassword());
        values.put("personEmail", person.getPersonEmail());
        values.put("personType", person.getPersonType());

        db.insert(TABLE_PERSON, null, values);
        db.close();
    }

    /**
     *
     * @return
     */
    public List<Person> getAllPersons() {
        List<Person> persons = new ArrayList<Person>();
        SQLiteDatabase db = this.getWritableDatabase();

        // String sqlQuery = "SELECT  * FROM " + TABLE_PERSON;
        // Cursor cursor = db.rawQuery(sqlQuery, null);

            Cursor cursor = db.query(TABLE_PERSON, new String[]{"id", "personName", "personPassword","personEmail",
                            "personType"},
                    null, null, null, null, null);
            while (cursor.moveToNext()) {
                Person person = new Person();
                person.setId(cursor.getInt(0));
                person.setPersonName(cursor.getString(1));
                person.setPersonPassword(cursor.getString(2));
                person.setPersonEmail(cursor.getString(3));
                person.setPersonType(cursor.getString(4));


                persons.add(person);
            }

        return persons;
    }

    /**
     *
     */
    public void resetTables(){
        //Bunuda uygulamada kullanmıyoruz. Tüm verileri siler. tabloyu resetler.
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_PERSON, null, null);
        db.close();
    }

    /**
     *
     */
    public void resetFoodsTable(){

        /// BU KOD BLOĞU YENİDEN GÖZDEN GECİRİLECEK
        //Bunuda uygulamada kullanmıyoruz. Tüm verileri siler. tabloyu resetler.
       SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_YEMEKLER, null, null);

        db.close();
    }

    /**
     *
     * @param yemek
     */
    public void insertFood(Yemekler yemek) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("foodName", yemek.getFoodName());
        values.put("foodPrice", yemek.getFoodPrice());
        values.put("siparisDurumu", yemek.getSiparisDurumu());


        db.insert(TABLE_YEMEKLER, null, values);
        db.close();
    }

    /**
     *
     * @param gelen_email
     * @return
     */
    public String getRegister(String gelen_email){

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor=db.query(TABLE_PERSON,null,  "personEmail=?",new String[]{gelen_email},null, null, null, null);
        if(cursor.getCount()<1){
            cursor.close();
            return "Not Exist";
        }
        else if(cursor.getCount()>=1 && cursor.moveToFirst()){

            password = cursor.getString(cursor.getColumnIndex("personPassword"));
            cursor.close();

        }


        return password;
    }

    /**
     *
     * @return
     */
    public String getSiparisPide(){
        String pide="";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor=db.query(TABLE_YEMEKLER,null,  "foodName=?",new String[]{"Pide"},null, null, null, null);
        if(cursor.getCount()<1){
            cursor.close();
            return "Not Exist";
        }
        else if(cursor.getCount()>=1 && cursor.moveToFirst()){

            pide= cursor.getString(cursor.getColumnIndex("foodName"));
            cursor.close();

        }


        return pide;
    }

    /**
     *
     * @return
     */
    public String getSiparisCorba(){
        String corba="";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor=db.query(TABLE_YEMEKLER,null,  "foodName=?",new String[]{"Corba"},null, null, null, null);
        if(cursor.getCount()<1){
            cursor.close();
            return "Not Exist";
        }
        else if(cursor.getCount()>=1 && cursor.moveToFirst()){

            corba= cursor.getString(cursor.getColumnIndex("foodName"));
            cursor.close();

        }


        return corba;
    }

    /**
     *
     * @return
     */
    public String getSiparisLahmacun(){
        String lahmacun="";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor=db.query(TABLE_YEMEKLER,null,  "foodName=?",new String[]{"Lahmacun"},null, null, null, null);
        if(cursor.getCount()<1){
            cursor.close();
            return "Not Exist";
        }
        else if(cursor.getCount()>=1 && cursor.moveToFirst()){

            lahmacun= cursor.getString(cursor.getColumnIndex("foodName"));
            cursor.close();

        }

        return lahmacun;
    }

    /**
     *
     * @return
     */
    public String getSiparisIskender(){
        String iskender="";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor=db.query(TABLE_YEMEKLER,null,"foodName=?",new String[]{"Iskender"},null,null,null,null);
        if (cursor.getCount() < 1) {
            cursor.close();
            return "Not Exist";
        }
        else if (cursor.getCount()>=1 && cursor.moveToFirst()){
            iskender = cursor.getString(cursor.getColumnIndex("foodName"));
            cursor.close();
        }
        return iskender;
    }


    /**
     *
     * @param useremail
     * @param password
     * @param type
     * @return
     * @throws SQLException
     */
    public boolean Login(String useremail, String password , String type) throws SQLException
    {   SQLiteDatabase db = this.getReadableDatabase();
        Cursor mCursor = db.rawQuery("SELECT * FROM " + TABLE_PERSON + " WHERE personEmail=? AND personPassword=? AND personType=?", new String[]{useremail,password,type});
        if (mCursor != null) {
            if(mCursor.getCount() > 0)
            {
                return true;
            }
        }
        return false;
    }
}
