package com.software.eng.restaurant;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by sevket&mcanv on 20/04/16.
 */
public class SignUp extends Activity {

    private String temp_name,temp_password,temp_email,temp_type;
    public static String GONDER_NAME="isim";
    public static String GONDER_PASSWORD="pass";
    public static String GONDER_MAİL="mail";
    public static String GONDER_TYPE="type";

    Button button_cancel,button_register,button_info;
    EditText eT_name,eT_password,eT_type,eT_email;
    CheckBox c_asci,c_garson;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signin);
        button_cancel = (Button) findViewById(R.id.b_cancel);
        button_register = (Button) findViewById(R.id.b_register);
        c_asci = (CheckBox) findViewById(R.id.c_asci);
        c_garson= (CheckBox) findViewById(R.id.c_garson);


        button_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eT_name=(EditText)findViewById(R.id.t_username);
                eT_password=(EditText) findViewById(R.id.t_password);
                eT_email=(EditText) findViewById(R.id.t_email);
                temp_name=String.valueOf(eT_name.getText().toString().trim());
                temp_password=String.valueOf(eT_password.getText().toString().trim());
                temp_email=String.valueOf(eT_email.getText().toString().trim());

                isGarsonChecked();
                isAsciChecked();

                if(isGarsonAndAsciChecked()){
                    Toast.makeText(getApplicationContext(),"Lütfen görevinizi seçiniz ",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(isNotGarsonAndAsciChecked()){
                    Toast.makeText(getApplicationContext(),"Aynı anda ikisini seçemezsiniz ",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(isRegisterInfoEntered()){
                    Toast.makeText(getApplicationContext(),"Lütfen eksik bilgilerinizi giriniz ",Toast.LENGTH_SHORT).show();
                    return;
                }

                Bundle bundle = new Bundle();
                    bundle.putString(GONDER_NAME, temp_name);
                    bundle.putString(GONDER_PASSWORD, temp_password);
                    bundle.putString(GONDER_TYPE, temp_type);
                    bundle.putString(GONDER_MAİL, temp_email);


                    Intent myintent = new Intent();
                    myintent.putExtras(bundle);
                    myintent.setClass(getApplicationContext(), RegInfo.class);
                    SignUp.this.startActivity(myintent);
            }
        });
        button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(SignUp.this,MainActivity.class);
                startActivity(myIntent);

            }
        });

    }
         void isGarsonChecked(){
            if(c_garson.isChecked()){
                temp_type="garson";
            }
        }
        void isAsciChecked(){
            if(c_asci.isChecked()){
                temp_type="asci";
             }
         }
        boolean isGarsonAndAsciChecked(){
            if(c_garson.isChecked()==false && c_asci.isChecked()==false){
                return true;
                }
        else return false;
         }
        boolean isNotGarsonAndAsciChecked(){
            if(c_garson.isChecked()==true && c_asci.isChecked()==true){
                return true ;
             }
        else return false;
         }
        boolean isRegisterInfoEntered(){
            if(eT_name.getText().toString().matches("") ||eT_password.getText().toString().matches("") ||
                eT_email.getText().toString().matches("") ){
                    return true;
             }
        else return false;
          }


}
