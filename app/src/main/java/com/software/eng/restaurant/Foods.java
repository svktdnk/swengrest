package com.software.eng.restaurant;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by sevket&mcanv on 30.04.2016.
 */
public class Foods extends Activity{

    Button b_siparis_al,b_siparisi_alma;
    String f_pide,f_corba,f_lahmacun,f_iskender;

    TextView textView;
    SeekBar  seekBar;

    int      f_pide_fiyati=0, f_corba_fiyati=0,f_lahmacun_fiyati=0,f_iskender_fiyati=0;
    CheckBox c_pide,c_corba,c_lahmacun,c_iskender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.foods_layout);
        b_siparis_al=(Button) findViewById(R.id.b_siparis_al);

        /*Gelecek güncelleştirmeler için yazıldı*/
        f_pide_fiyati=3;
        f_corba_fiyati=5;
        f_lahmacun_fiyati=10;
        f_iskender_fiyati=20;


        seekBar = (SeekBar) findViewById(R.id.sB_pide);
        textView = (TextView) findViewById(R.id.tV_pide);
        // Set SeekBar Thumbnail Programmatically
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){

            int seekBarProgress = 0;

            /**
             *
             * @param seekBar
             * @param progress
             * @param fromUser
             */
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
                seekBarProgress = progress;
            }

            /**
             *
             * @param seekBar
             */
            public void onStartTrackingTouch(SeekBar seekBar){

            }

            /**
             *
             * @param seekBar
             */
            public void onStopTrackingTouch(SeekBar seekBar){
                textView.setText("Seçilen : " + seekBarProgress + " / " + seekBar.getMax());
            }


        });

        b_siparis_al.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                c_pide=(CheckBox) findViewById(R.id.c_pide);
                c_corba=(CheckBox) findViewById(R.id.c_corba);
                c_lahmacun= (CheckBox) findViewById(R.id.c_lahmacun);
                c_iskender= (CheckBox) findViewById(R.id.c_iskender);


                isPide();
                isCorba();
                isLahmacun();
                isIskender();



                DBHelper dbHelper = new DBHelper(getApplicationContext());
                SharedPreferences settings = getSharedPreferences("SQL", 0);
                boolean firstTime = settings.getBoolean("firstTime", true);
                 // RESETFOODSTABLE FONKSİYONU CALİSMİYOR
                   // dbHelper.resetFoodsTable();
                SQLiteDatabase sqldb;
                   sqldb=dbHelper.getWritableDatabase();
                String sql2=" CREATE TABLE IF NOT EXISTS " + "Yemekler" + "(id INTEGER PRIMARY KEY AUTOINCREMENT , " +
                        "foodName TEXT , " +
                        "foodPrice TEXT ," +
                        "siparisDurumu  TEXT " + ")";
                Log.d("DBHelper", "SQL : " + sql2);
                try {
                    sqldb.execSQL(sql2);
                    dbHelper.resetFoodsTable();
                    dbHelper.insertFood(new Yemekler(f_pide, String.valueOf(f_pide_fiyati), "true"));
                    dbHelper.insertFood(new Yemekler(f_corba, String.valueOf(f_corba_fiyati), "true"));
                    dbHelper.insertFood(new Yemekler(f_lahmacun,String.valueOf(f_lahmacun_fiyati),"true"));
                    dbHelper.insertFood(new Yemekler(f_iskender,String.valueOf(f_iskender_fiyati),"true"));
                    Toast.makeText(getApplicationContext()," Sipariş aşçıya iletildi " ,Toast.LENGTH_SHORT).show();

                }catch (SQLException e) {
                    Toast.makeText(getApplicationContext()," Bir sorun oluştu " ,Toast.LENGTH_SHORT).show();

                }

            }
        });
 /*     b_siparisi_alma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Foods.this,MainActivity.class);
                Foods.this.startActivity(myIntent);
            }
        });   */
    }


    void isPide(){
        if(c_pide.isChecked()){
            f_pide="Pide";
        }
    }
    void isCorba(){
        if(c_corba.isChecked()){
            f_corba="Corba";
        }
    }
    void isLahmacun(){
        if(c_lahmacun.isChecked()){
            f_lahmacun="Lahmacun";
        }
    }
    void isIskender(){
        if (c_iskender.isChecked()){
            f_iskender="Iskender";
        }
    }

}
