package com.software.eng.restaurant;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;


/**
 *
 */
public class MyListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private List<Person> personList;

    /**
     *
     * @param activity
     * @param persons
     */
    public MyListAdapter(Activity activity, List<Person> persons) {

        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
         personList = persons;
    }

    /**
     *
     * @return
     */
    @Override
    public int getCount() {
        return personList.size();
    }

    /**
     *
     * @param position
     * @return
     */
    @Override
    public Object getItem(int position) {
        return personList.get(position);
    }

    /**
     *
     * @param position
     * @return
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     *
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        if (convertView == null)
            vi = inflater.inflate(R.layout.listview_rows, null); // create layout from



        TextView tV_person_name=(TextView)vi.findViewById(R.id.tV_person_name);
        TextView tV_person_password=(TextView)vi.findViewById(R.id.tV_person_password);
        TextView tV_person_type=(TextView)vi.findViewById(R.id.tV_person_type);
        TextView tV_person_email=(TextView)vi.findViewById(R.id.tV_person_email);
         Person person = personList.get(position);
        tV_person_name.setText(String.valueOf(personList.get(position).getPersonName()));
        tV_person_password.setText( String.valueOf(personList.get(position).getPersonPassword()) );
        tV_person_type.setText(String.valueOf(personList.get(position).getPersonType()));
        tV_person_email.setText(String.valueOf(personList.get(position).getPersonEmail()));

        return vi;
    }
}
