package com.software.eng.restaurant;

import java.io.Serializable;

/**
 * Created by sevket&mcanv on 19.04.2016.
 */

/**
 *
 */
public class Person  implements Serializable {
    private static final long serialVersionUID = 1L;
    private int id;
    private String personName;
    private String personEmail;
    private String personPassword;
    private String personType;

    /**
     *
     */
    public Person(){
        super();
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id){this.id=id;}

    /**
     *
     * @return
     */
    public String getPersonName() {
        return personName;
    }

    /**
     *
     * @param personName
     */
    public void setPersonName(String personName) {
        this.personName = personName;
    }

    /**
     *
     * @return
     */
    public String getPersonEmail() {
        return personEmail;
    }

    /**
     *
     * @param personEmail
     */
    public void setPersonEmail(String personEmail) {
        this.personEmail = personEmail;
    }

    /**
     *
     * @return
     */
    public String getPersonPassword() {
        return personPassword;
    }

    /**
     *
     * @param personPassword
     */
    public void setPersonPassword(String personPassword) {
        this.personPassword = personPassword;
    }

    /**
     *
     * @return
     */
    public String getPersonType() {
        return personType;
    }

    /**
     *
     * @param personType
     */
    public void setPersonType(String personType) {
        this.personType = personType;
    }

    /**
     *
     * @param personName
     * @param personPassword
     * @param personType
     * @param personEmail
     */
    public Person(String personName, String personPassword ,String personType , String personEmail ) {
       super();
        this.personName      =personName;
        this.personPassword  =personPassword;
        this.personType      =personType;
        this.personEmail     =personEmail;


    }

}
