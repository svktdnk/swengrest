package com.software.eng.restaurant;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by sevket&mcanv on 21.04.2016.
 */
public class RegInfo extends Activity {
    private DBHelper persons;
    private  String gelen_name,gelen_password,gelen_email ,gelen_type;
    Bundle b;

    //RegInfo Buttons
    Button b_login_page;
    Button b_edit;
    Button b_cancel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reg_informations);
        b=getIntent().getExtras();
        gelen_name=b.getString(SignUp.GONDER_NAME);
        gelen_password=b.getString(SignUp.GONDER_PASSWORD);
        gelen_email=b.getString(SignUp.GONDER_MAİL);
        gelen_type=b.getString(SignUp.GONDER_TYPE);

        final ListView customListView = (ListView) findViewById(R.id.lV_reg_informations);
        DBHelper dbHelper = new DBHelper(getApplicationContext());

        SharedPreferences settings = getSharedPreferences("SQL", 0);
        boolean firstTime = settings.getBoolean("firstTime", true);
        //dbHelper.resetTables();
        try {
            dbHelper.resetTables();
            dbHelper.insertPerson(new Person(
                    gelen_name.toString().trim(),
                    gelen_password.toString().trim(),
                    gelen_type.toString().trim(),
                    gelen_email.toString().trim()));
            Toast.makeText(getApplicationContext()," Kayıt gerçekleştirildi " ,Toast.LENGTH_SHORT).show();
        }catch (Exception e){
            Toast.makeText(getApplicationContext()," Kayıt gerçekleşmedi lütfen bilgileri kontrol ediniz" ,Toast.LENGTH_SHORT).show();

        }
        SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("firstTime", false);
                editor.commit();

           /* else
           {
               dbHelper.insertPerson(new Person(
                       gelen_name.toString(),
                       gelen_password.toString(),
                       gelen_email.toString(),
                       gelen_type.toString()));

           } */

            List<Person> persons = dbHelper.getAllPersons();
            MyListAdapter myListAdapter = new MyListAdapter(RegInfo.this, persons);
            customListView.setAdapter(myListAdapter);

           // Toast.makeText(getApplicationContext(), "Kayıt  yapıldı", Toast.LENGTH_SHORT).show();
           // Toast.makeText(getApplicationContext(), "Bir sorun oluştu lütfen gözden geçiriniz", Toast.LENGTH_SHORT).show();

        b_login_page= (Button) findViewById(R.id.b_loginPage);
        b_login_page.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(RegInfo.this,MainActivity.class);
                startActivity(myIntent);
            }
        });
        b_edit= (Button) findViewById(R.id.b_edit);
        b_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(RegInfo.this,SignUp.class);
                startActivity(myIntent);
            }
        });

    }
}
