package com.software.eng.restaurant;

import java.io.Serializable;

/**
 * Created by sevket&mcanv on 01.05.2016.
 */

/**
 *
 */
public class Yemekler implements Serializable {
   // private static final long serialVersionUID = 1L;
    private int id;
    private String foodName;
    private String foodPrice;
    private String siparisDurumu;

    /**
     *
     */
    public Yemekler() {
        super();
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getFoodName() {
        return foodName;
    }

    /**
     *
     * @param foodName
     */
    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    /**
     *
     * @return
     */
    public String getFoodPrice() {
        return foodPrice;
    }

    /**
     *
     * @param foodPrice
     */
    public void setFoodPrice(String foodPrice) {
        this.foodPrice = foodPrice;
    }

    /**
     *
     * @return
     */
    public String getSiparisDurumu() {
        return siparisDurumu;
    }

    /**
     *
     * @param siparisDurumu
     */
    public void setSiparisDurumu(String siparisDurumu) {
        this.siparisDurumu = siparisDurumu;
    }

    /**
     *
     * @param foodName
     * @param foodPrice
     * @param siparisDurumu
     */
    public Yemekler( String foodName, String foodPrice, String siparisDurumu) {
         super();
        this.foodName = foodName;
        this.foodPrice = foodPrice;
        this.siparisDurumu = siparisDurumu;
    }
}
