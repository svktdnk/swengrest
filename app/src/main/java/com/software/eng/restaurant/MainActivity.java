package com.software.eng.restaurant;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by sevket&mcanv on 10.04.2016.
 */

public class MainActivity extends Activity {

    Button b_login, b_cancel ,b_signIn ;
    EditText eT_username , eT_password;

    TextView t_cancel;
    int  counter = 3 ;


    //myIntent tıklayınca Sıgn In sınıfınında işlem yapacağımız anlamına geliyor


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("csd", "onCreate çalıştı aktivite nesnesi yaratıldı");
        setContentView(R.layout.activity_main);

        b_login  = (Button) findViewById(R.id.loginButton);
        eT_username = (EditText) findViewById(R.id.usernameText);
        eT_password = (EditText) findViewById(R.id.passwordText);


        b_cancel = (Button) findViewById(R.id.cancelButton);
        t_cancel = (TextView) findViewById(R.id.cancelText);
        t_cancel.setVisibility(View.GONE);

        b_signIn = (Button) findViewById(R.id.b_signIn);

        b_login.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                DBHelper db=new DBHelper(getApplicationContext());
                String girilen_email,girilen_sifre,girilen_garson,girilen_asci;
                girilen_email=eT_username.getText().toString();
                girilen_sifre=eT_password.getText().toString();
                girilen_garson="garson";
                girilen_asci="asci";
                Intent Tables_class_git=new Intent(MainActivity.this , Tables.class);
                Intent Asci_class_git=new Intent(MainActivity.this , Asci.class);

               // String StoredPassword=db.getRegister(eT_username.getText().toString());
                boolean loginGarson=db.Login(girilen_email,girilen_sifre,girilen_garson);
                boolean loginAsci=db.Login(girilen_email,girilen_sifre,girilen_asci);
                if(loginGarson){

                    Toast.makeText(getApplicationContext(), "Bağlanıyor...", Toast.LENGTH_SHORT).show();

                    MainActivity.this.startActivity(Tables_class_git);

                }
                else if(loginAsci)
                {
                    Toast.makeText(getApplicationContext(), "Bağlanıyor...", Toast.LENGTH_SHORT).show();
                    MainActivity.this.startActivity(Asci_class_git);
                }
                else {
                    Toast.makeText(getApplicationContext(), "Yanlış şifre ", Toast.LENGTH_SHORT).show();

                    t_cancel.setVisibility(View.VISIBLE);
                    t_cancel.setBackgroundColor(Color.BLUE);
                    counter--;
                    t_cancel.setText(Integer.toString(counter));

                    if(counter == 0){
                        b_login.setEnabled(false);
                    }
                }
            }
        });
        /**
         * Cancel butonuna tıklayınca giriş yapmayacak
         */
        b_cancel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                finish();
            }
        });
        /**
         * Sıgn In 'e tıklayınca yeni bir activity'e yönlendirecek
         */
        b_signIn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MainActivity.this,SignUp.class);
                MainActivity.this.startActivity(myIntent);
            }
        });
    }


       /* LinearLayout root = new LinearLayout(this);
            root.setOrientation(LinearLayout.VERTICAL);

        Button child1 = new Button(this);
        child1.setText("Click me");
        child1.setWidth(200);
        child1.setHeight(View.FOCUS_BACKWARD);
        root.addView(child1);

        CheckBox child2 = new CheckBox(this);
        child2.setText("Düğme");
        root.addView(child2);

        TextView child3 = new TextView(this);
        child3.setText("Programatik yöntem");
        child3.setHeight(40);
        child3.setWidth(200);
        root.addView(child3);

        this.setContentView(root);*/
    /* ----------------------------------------------*/
  /*  @Override
    protected void onResume(){
        super.onResume();
        Log.e("csd", "onResume çalıştı aktivite stacte ön plana geldi");
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        Log.e("csd","onDestroy çalıştı aktivite nesnesi yok edildi");
    }
    @Override
    protected void onPause(){
        super.onPause();
        Log.e("csd","onPause çalıştı aktivite stacte arla plana gitti");
    }
    @Override
    protected void onRestart(){
        super.onRestart();
        Log.e("csd","onRestart çalıştı ve aktivite yeniden çalıştı");
    }
    @Override
    protected void onStart(){
        super.onStart();
        Log.e("csd","onStart çalıştı ve aktivite çalıştı");
    }
    @Override
    protected void onStop(){
        super.onStop();
        Log.e("csd","onStop çalıştı ve aktivite durdu");
    }*/
}
