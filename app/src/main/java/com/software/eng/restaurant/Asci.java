package com.software.eng.restaurant;

import android.app.Activity;
import android.database.SQLException;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by sevket&mcanv on 11.05.2016.
 */

/**
 *
 */
public class Asci extends Activity {

    TextView  t_siparis_1,t_siparis_2,t_siparis_3,t_siparis_4;
    Button b_siparis_hazir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asci_layout);

        t_siparis_1=(TextView) findViewById(R.id.t_siparis_1);
        t_siparis_2=(TextView) findViewById(R.id.t_siparis_2);
        t_siparis_3= (TextView) findViewById(R.id.t_siparis_3);
        t_siparis_4= (TextView) findViewById(R.id.t_siparis_4);

        b_siparis_hazir=(Button) findViewById(R.id.b_siparis_hazir);
        DBHelper db= new DBHelper(getApplicationContext());
        try {
            t_siparis_1.setText(db.getSiparisPide());
            t_siparis_2.setText(db.getSiparisCorba());
            t_siparis_3.setText(db.getSiparisLahmacun());
            t_siparis_4.setText(db.getSiparisIskender());

        }catch (SQLException e){

        }
        b_siparis_hazir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DBHelper db=new DBHelper(getApplicationContext());
                try {
                    db.resetFoodsTable();
                    Toast.makeText(getApplicationContext(),"Siparis teslim edildi" , Toast.LENGTH_SHORT).show();
                }catch (SQLException e){

                    Toast.makeText(getApplicationContext(),"Bir sorun oluştu" , Toast.LENGTH_SHORT).show();

                }
            }
        });


    }
}
